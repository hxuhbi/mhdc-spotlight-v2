vf <- function(vol,activeyr,forecastyr){
  for(i in activeyr:(activeyr+forecastyr-1)){
   vol[i+1] <- ycdpj(n=activeyr,VD=vol[(i-activeyr+1):i])
  }
  if(activeyr+forecastyr<length(vol)){
    vol[(activeyr+forecastyr+1):length(vol)] <- NA
  }
  return(vol)
}

vf.err <- function(vol,activeyr,forecastyr){
  err <- rep(NA,length(vol))
  err[activeyr] <- 0
  for(i in 1:forecastyr){
    vol[i+activeyr] <- ycdpj(n=activeyr, VD=vol[i:(activeyr+i-1)])
    tmp <-  mean(c(vol[activeyr+i-1], vol[activeyr+i-2]));
    err[activeyr+i] <- abs(vol[activeyr+i]-tmp)/tmp
  }
  return(err)
}